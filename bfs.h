#include <queue>

using std::queue;

enum Command
{
				And,
				Nand,
				Or,
				Nor,
				Xor,
				Xnor,
				build_and,
				build_nand,
				build_or,
				build_nor,
				build_xor,
				build_xnor,
				FindRoot,
				Replace,
				Free,
				del,
				InversionToMarks,
				MarksToInversion,
				CountWires,
				MakeLines,
				UpdateWires,
				NumberElements
};

void PushInversion (Node *node);

/*
===========================================================
Returns true if we found something/need to stop search.
Can do whatever you need if you add appropriate case.
===========================================================
*/
Node* Visit(Node * node, int task, int set);


/*
===========================================================
Inputs are the root node of the scheme to process,
string that specifies which visitor function to use
and integer of what set to look for. (Needed for main task)
==============================================Node *root
*/
Node* BFS(Node *root, int task, int set = 0);
