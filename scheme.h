#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string.h>


using std::vector;
using std::string;

class Node
{
public:
	string type;
	bool is_visited; // shows which elements were visited during bfs
	int             uid;
	vector<class Node *> parents;
	vector<class Node *> children;
	vector<bool>		 input_inversion;
	void			AddParent		(Node *node);
	void			AddChild		(Node *node);
	void			DeleteChild		(int i);
	void			DeleteParent	(int i);
	int  			InputPins		();
	int  			OutputPins		();
	int  			Function		(vector<int> v);
	void 			AddToSet    	(int set_num);
	bool 			IsInSet			(int set_num);
	static int 		MaxSet			();
	static void 	UpSets			(int i);
	void			InvertType		();
	static vector<int> num;
	vector<int> sets; // shows which sets this node belongs to
};

class Wire
{
public:
	string          type;
	Node*           parent;
	Node*           child;
	int             p;
	int             c;
	int             id;
};

class Scheme
{
public:
	int 			UniqueInputNumber(); // make this to improve accuracy
	vector<Node *>  input;
	vector<Node *>  output;
	vector<Wire *>  wires;
	Node*			root;
	int 			SizeOfInput		(); // returns size of input pins
	int 			SizeOfOutput	(); // returns size of output pins
	class Scheme    SubScheme		(vector<Node *> in, Node *out); // makes copy of a part
	int 			Process			(vector<int> v);// returns output of a scheme with given input. for lulz
	void		    PlaceMacro		(Node *macro);// instead of number of set to look for
	Node*			GetRoot			(int set);
	void 			Init			(char* path); 	   // creates main scheme with info from parser;
	void			CreateVerilog	(char* path);
	void			FreeMemory		();
	void			ConvertInversionsToMarks();
	void			ConvertMarksToInversions();
	bool            IsInput(string line);
	string	 		GetInputName	();
    string	 		GetOutputName	();
    int             LetsCount       (string line, string flag);
    //int             FindWire        (Node * node, string line);

};

void InitMacro(Node * macro, char * path);
string IntToString(int  a);
int FindWire (Node * node, string line, int p = 0);
