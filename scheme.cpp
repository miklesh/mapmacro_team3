#include "scheme.h"
#include "bfs.h"

using namespace std;
#define uint unsigned int

vector<int> Node::num;

int Node::Function(vector<int> v)
{
	if (type == "and")
	{
		for (uint i = 0; i < v.size(); i++)
		{
			if (v[i] == 0)
			{
				return 0;
			}
		}
		return 1;
	}
	else if (type == "nand")
	{
		for (uint i = 0; i < v.size(); i++)
		{
			if (v[i] == 0)
			{
				return 1;
			}
		}
		return 0;
	}
	else if (type == "or")
	{
		for (uint i = 0; i < v.size(); i++)
		{
			if (v[i] == 1)
			{
				return 1;
			}
		}
		return 0;
	}
	else if (type == "nor")
	{
		for (uint i = 0; i < v.size(); i++)
		{
			if (v[i] == 1)
			{
				return 0;
			}
		}
		return 1;
	}
	else if (type == "xor")
	{
		int x = 0;
		for (uint i = 0; i < v.size(); i++)
		{
			x += v[i];
		}
		x %= 2;
		return x;
	}
	else if (type == "xnor")
	{
		int x = 0;
		for (uint i = 0; i < v.size(); i++)
		{
			x += v[i];
		}
		x += 1;
		x %= 2;
		return x;
	}
	else if (type == "not")
	{
		if (v[0])
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	return 100500;
}

int Node::InputPins ()
{
	return parents.size();
}

int Node::OutputPins ()
{
	return children.size();
}

bool Node::IsInSet (int set_num)
{
	unsigned int i;
	for (i = 0; i < sets.size(); i++)
	{
		if (sets[i] == set_num)
		{
			return true;
		}
	}
	return false;
}

void Node::UpSets(int i)
{
    //std::cout << num.size() -1<< " "<<i<<" ";
	if (int (num.size()) - 1 < i)
	{
		num.push_back(0);
	}
    num[i]++;
    //std::cout<<num[i]<<std::endl;
}

void Node::AddToSet (int set_num)
{
	if (!IsInSet(set_num))
	{
		UpSets(set_num);
		sets.push_back(set_num);
	}
}

int Scheme::SizeOfInput ()
{
	return input.size();
}

int Scheme::SizeOfOutput ()
{
	return  output.size();
}

void Node::AddParent (Node *node)
{
	parents.push_back(node);
	node->children.push_back(this);
	input_inversion.push_back(false);
}

void Node::AddChild (Node *node)
{
	children.push_back(node);
	node->parents.push_back(this);
	node->input_inversion.push_back(false);
}

int Node::MaxSet()
{
	int max = 0, idx = -1;
	for (unsigned int i = 0; i < num.size(); i++)
	{
		if (num[i] > max)
		{
			idx = i;
			max = num[i];
		}
	}
	return idx;
}

void Node::InvertType()
{
	if (type == "and")
	{
		type = "nand";
	}
	else if (type == "nand")
	{
		type = "and";
	}
	else if (type == "or")
	{
		type = "nor";
	}
	else if (type == "nor")
	{
		type = "or";
	}
	else if (type == "xor")
	{
		type = "xnor";
	}
	else if (type == "xnor")
	{
		type = "xor";
	}
}

int EmptyInputs(Node * macro)
{
	int res = 0;
	for (int i = 0; i < macro->InputPins(); i++)
	{
		if (macro->parents[i] == NULL)
		{
			res++;
		}
	}
	return res;
}

void AddAllParentsX(Node * node, Node * macro, int set)
{
	int i;
	for (i = 0; i < node->InputPins(); i++)
	{
		if (node->parents[i]->IsInSet(set))
		{
			AddAllParentsX(node->parents[i], macro, set);
		}
		else
		{
			if (EmptyInputs(macro) > 0)
			{
				macro->parents.erase(macro->parents.begin());
				macro->AddParent(node->parents[i]);
			}
		}
	}
}

void AddAllChildrenX(Node * node, Node * macro, int set)
{
	int i;
	for (i = 0; i < node->OutputPins(); i++)
	{
		macro->AddChild(node->children[i]);
	}
}

void Scheme::FreeMemory()
{
	BFS(root, Free);
}

void Node::DeleteChild(int i)
{
	Node *node = children[i];
	int j;
	if (node != NULL)
	{
		for (j = 0; j < node->InputPins(); j++)
		{
			if (node->parents[j] == this)
			{
				break;
			}
		}
		children.erase(children.begin() + i);
		node->parents.erase(node->parents.begin() + j);
	}
}

void Node::DeleteParent(int i)
{
	Node *node = parents[i];
	int j;
	if (node != NULL)
	{
		for (j = 0; j < node->OutputPins(); j++)
		{
			if (node->children[j] == this)
			{
				break;
			}
		}
		parents.erase(parents.begin() + i);
		node->children.erase(node->children.begin() + j);
	}
}

void DeleteNodes(Node *node,int set)
{
	BFS(node, del, set);
}

Node* MakeConst(Node* node)
{
	Node* in = node;
	Node* p1;
	Node* p2;
	/* Next line needed because if we actually need
	to add constant then it stays in the beggining
	which caused programm to crash */
	in = in->parents[in->InputPins() - 1];
	while (in->InputPins() != 0)
	{
		in = in->parents[0];
	}
	if ((node->type == "and") || (node->type == "nand"))
	{
		p1 = new Node;
		p1->type = "not";
		p1->AddParent(in);
		p2 = new Node;
		p2->type = "or";
		// making const 1
		p2->AddParent(p1);
		p2->AddParent(in);
		return p2;
	}
	else
	{
		p1 = new Node;
		p1->type = "not";
		p1->AddParent(in);
		p2 = new Node;
		p2->type = "and";
		// making const 0
		p2->AddParent(p1);
		p2->AddParent(in);
		return p2;
	}
}

void FixExtraInputs(Node *node)
{
	int i;
	bool iNeedToFixIt = false;
	if (EmptyInputs(node) > 0)
	{
	    iNeedToFixIt = true;
	}
	if (iNeedToFixIt)
	{
		Node* constant;
		constant = MakeConst(node);
		for (i = 0; i < node->InputPins(); i++)
		{
			if (node->parents[i] == NULL)
			{
				constant->children.push_back(node);
				node->parents[i] = constant;
			}
		}
	}
}

void Scheme::PlaceMacro(Node *macro)
{
	Node *node;
	int set = root->MaxSet();
	node = BFS(root, FindRoot, set);
	AddAllParentsX (node, macro, set);
	AddAllChildrenX(node, macro, set);
	for (int i = 0; i < node->OutputPins(); i++)
    {
        node->DeleteChild(i);
    }
	DeleteNodes(node, set);
	FixExtraInputs(macro);
}



void SkipLine(ifstream& f)
{
    char c='*';
    string line;
    while (c != ';')
    {
        f >> line;
        c = line[line.length() - 1];
    }
}

int GetValue(string line)
{
    int result = 0;
    char c[20];
    size_t length = line.copy(c,line.size() - 2,1);
    c[length]='\0';
    sscanf(c, "%d", &result);
    return result;
 }

int inCounter, outCounter, wireCounter;

Node* CreateInputNode(string line)
{
    char c[20];
	Node *p;
    p = new Node;
    line += "[";
    sprintf(c, "%d", inCounter++);
    line += c;
    line += "]";
    p->type = line;
    return p;
}

Node* CreateOutputNode(string line)
{
    char c[20];
	Node *p;
    p = new Node;
    line += "[";
    sprintf(c, "%d", outCounter++);
    line += c;
    line += "]";
    p->type = line;
    return p;
}

Wire* CreateWire(string line)
{
    char c[20];
	Wire *p;
    p = new Wire;
    line += "[";
    sprintf(c, "%d", wireCounter++);
    line += c;
    line += "]";
    p->type = line;
    return p;
}
/*
void ConnectWires(string line, Node* p)
{
    Node *p;
    p = new Node;
    p->type = "and";
    f >> line;
    for (i = 0; i < 100; i++)
    {
        str[i] = '\0';
    }
    line.copy(str, line.size()- 2, 1);
    line = str;
    for (i = 0; i < int (wires.size()); i++)
    {
        if (wires[i]->type == line)
        {
            wires[i]->parent = p;
            break;
        }
    }
    f >> line;
    for (i = 0; i < 100; i++)
    {
        str[i] = '\0';
    }
    line.copy(str, line.size() - 1, 0);
    line = str;
    for (i = 0; i < int (wires.size()); i++)
    {
        if (wires[i]->type == line)
        {
            wires[i]->child = p;
            break;
        }
    }
    f >> line;
    for (i = 0; i < 100; i++)
    {
        str[i] = '\0';
    }
    line.copy(str, line.size() - 2, 0);
    line = str;
    for (i = 0; i < int (wires.size()); i++)
    {
        if (wires[i]->type == line)
        {
            wires[i]->child = p;
            break;
        }
    }
}
*/

bool Scheme::IsInput(string line)
{
    int i;
    for (i = 0; i < SizeOfInput(); i++)
    {
        if (input[i]->type == line)
        {
            return true;
        }
    }
}

void Scheme::Init(char* path)
{
    Node *p;
    Wire *pw;
    int i, j, comas = 0,k;
    char str[200];
    char c = 0;
    inCounter = outCounter = wireCounter = 0;
	ifstream f;
	f.open(path);
	string line, temp;
	SkipLine(f); // Skip head line.
	line = "start";
	while (line != "endmodule")
	{
	    /*
	    Next block handles strings like
	    -----------------
	    input  [2:0] a, b;
        input        sel;
        -----------------
        NB: Never write multiple declarations without space after coma.
        This will lead to unexpectable consequences.
        (Normally it just wouldn't get real names of input pins
         and their number which is bad.)
        */
        /*
                                CAUTION!!!
                The following part of code may be inappropriate for
                self-respecting programmers and must be completely
                                REWRITTEN.
                Try to understand on your own risk.
                Do not write like this at home.
        */
        f >> line;
	    if (line == "input")
        {
        	comas = 0;
            inCounter = 0;
            f >> line;
            j = 0;
            // This block goes if input is an array
            if (line[0] == '[')
            {
            	// This thing converts array of chars to its meant integer value
                j = GetValue(line) + 1;
                // Get the name of input now
                f >> line;
            }
            if (line[line.size() - 1] == ',')
            {
                comas = 1;
            }
            do
            {
                if (line[line.size() - 1] == ';')
                {
                    comas = 0;
                }
                line.erase(line.end()-1);
                // This goes for array
                for (k = 0; k < j; k++)
                {
                    p = CreateInputNode(line);
                    input.push_back(p);
                    //cout << input[input.size()-1]->type <<endl;
                }
                // and this for single input
                if (j == 0)
                {
                    p = new Node;
                    p->type = line;
                    input.push_back(p);
                    //cout << input[input.size()-1]->type << endl;
                }
                inCounter = 0;
                if (comas == 0)
                {
                    comas = 1;
                    break;
                }
                f >> line;
            }
            while (comas != 0);
            if (comas == 0)
            {
            	// This goes for array
	            for (i = 0; i < j; i++)
	            {
	            	p = CreateInputNode(line);
	            	input.push_back(p);
	                //cout << input[input.size()-1]->type <<endl;
	            }
	            // and this for single input
	            if (j == 0)
	            {
	                p = new Node;
	                //line[line.size() - 1] = '\0';
	                line.erase(line.end()-1);
	                p->type = line;
	                input.push_back(p);
	                //cout << input[input.size()-1]->type << endl;
	            }
            }
        }
        else if (line == "output")
        {
        	comas = 0;
            outCounter = 0;
            f >> line;
            j = 0;
            // This block goes if input is an array
            if (line[0] == '[')
            {
            	// This thing converts array of chars to its meant integer value
                j = GetValue(line) + 1;
                // Get the name of input now
                f >> line;
            }
            if (line[line.size() - 1] == ',')
            {
                comas = 1;
            }
            //line[line.size() - 1] = '\0';
            //line.erase(line.end()-1);
            do
            {
                if (line[line.size() - 1] == ';')
                {
                    comas = 0;
                }
                //line[line.size() - 1] = '\0';
                line.erase(line.end()-1);
                // This goes for array
                for (k = 0; k < j; k++)
                {
                    p = CreateOutputNode(line);
                    output.push_back(p);
                    //cout << output[output.size()-1]->type <<endl;
                }
                // and this for single input
                if (j == 0)
                {
                    p = new Node;
                    p->type = line;
                    output.push_back(p);
                    //cout << output[output.size()-1]->type << endl;
                }
                outCounter = 0;
                if (comas == 0)
                {
                    comas = 1;
                    break;
                }
                f >> line;
            }
            while (comas != 0);
            if (comas == 0)
            {
            	// This goes for array
	            for (i = 0; i < j; i++)
	            {
	            	p = CreateOutputNode(line);
	            	output.push_back(p);
	                //cout << output[output.size()-1]->type <<endl;
	            }
	            // and this for single input
	            if (j == 0)
	            {
	                p = new Node;
	                //line[line.size() - 1] = '\0'
	                line.erase(line.end()-1);
	                p->type = line;
	                output.push_back(p);
	                //cout << output[output.size()-1]->type << endl;
	            }
            }
        }
        else if (line == "wire")
        {
            comas = 0;
            wireCounter = 0;
            f >> line;
            j = 0;
            // This block goes if input is an array
            if (line[0] == '[')
            {
            	// This thing converts array of chars to its meant integer value
                j = GetValue(line) + 1;
                // Get the name of input now
                f >> line;
            }
            if (line[line.size() - 1] == ',')
            {
                comas = 1;
            }
            //line[line.size() - 1] = '\0';
            do
            {
                if (line[line.size() - 1] == ';')
                {
                    comas = 0;
                }
                //line[line.size() - 1] = '\0';
                line.erase(line.end()-1);
                // This goes for array
                for (k = 0; k < j; k++)
                {
                    pw = CreateWire(line);
                    if (!IsInput(pw->type))
                        wires.push_back(pw);
                    else
                        delete pw;
                    //cout << wires[wires.size()-1]->type <<endl;
                }
                // and this for single input
                if (j == 0)
                {
                    pw = new Wire;
                    pw->type = line;
                    if (!IsInput(line))
                        wires.push_back(pw);
                    else
                        delete pw;
                    //cout << wires[wires.size()-1]->type << endl;
                }
                inCounter = 0;
                if (comas == 0)
                {
                    comas = 1;
                    break;
                }
                f >> line;
            }
            while (comas != 0);
            // btw thing below never executes
            if (comas == 0)
            {
            	// This goes for array
	            for (i = 0; i < j; i++)
	            {
	            	pw = CreateWire(line);
	            	wires.push_back(pw);
	                //cout << wires[wires.size()-1]->type <<endl;
	            }
	            // and this for single input
	            if (j == 0)
	            {
	                pw = new Wire;
	                //line[line.size() - 1] = '\0';
	                line.erase(line.end()-1);
	                pw->type = line;
	                wires.push_back(pw);
	                //cout << wires[wires.size()-1]->type << endl;
	            }
            }
        }
        else if (line == "not")
        {
            p = new Node;
            p->type = "not";
            f >> line;
            for (i = 0; i < 100; i++)
            {
                str[i] = '\0';
            }
            line.copy(str, line.size()- 2, 1);
            line = str;
            //line += c;
            for (i = 0; i < int (wires.size()); i++)
            {
                if (wires[i]->type == line)
                {
                    wires[i]->parent = p;
                    break;
                }
            }
            f >> line;
            for (i = 0; i < 100; i++)
            {
                str[i] = '\0';
            }
            line.copy(str, line.size() - 2, 0);
            line = str;
            //line += c;
            if (IsInput(line))
            {
                for (i = 0; i < SizeOfInput(); i++)
                {
                    if (input[i]->type == line)
                    {
                        input[i]->AddChild(p);
                    }
                }
            }
            else
            {
                for (i = 0; i < int (wires.size()); i++)
                {
                    /*cout << wires[i]->type<<" "<<line<<endl;
                    cout << wires[i]->type.size()<<" "<<line.size()<<endl;
                    cout << line.compare(wires[i]->type)<<endl;
                    for (j = 0; j < int(line.size()); j++)
                    {
                        cout << wires[i]->type[j]<<" != " <<line[j] << endl;
                    }*/
                    if (wires[i]->type == line)
                    {
                        wires[i]->child = p;
                        break;
                    }
                }
            }
        }
        else if (line != "endmodule")
        {
            p = new Node;
            p->type = line;
            f >> line;
            for (i = 0; i < 100; i++)
            {
                str[i] = '\0';
            }
            line.copy(str, line.size()-2, 1);
            line = str;
            //line += c;
            for (i = 0; i < int (wires.size()); i++)
            {
                //cout <<(wires[i]->type)<<" is "<<wires[i]->type.size()<<" "<< line <<" is "<<line.size()<<endl;
                //cout << int(wires[i]->type[2]) << endl;
                if (wires[i]->type == line)
                {
                    wires[i]->parent = p;
                    //cout<<wires[i]->parent->type<<endl;
                    break;
                }
            }
            f >> line;
            for (i = 0; i < 100; i++)
            {
                str[i] = '\0';
            }
            line.copy(str, line.size() - 1, 0);
            line = str;
            //line += c;
            if (IsInput(line))
            {
                for (i = 0; i < SizeOfInput(); i++)
                {
                    if (input[i]->type == line)
                    {
                        input[i]->AddChild(p);
                    }
                }
            }
            else
            {
                for (i = 0; i < int (wires.size()); i++)
                {
                    //cout <<(wires[i]->type == line)<<endl;
                    if (wires[i]->type == line)
                    {
                        wires[i]->child = p;
                        break;
                    }
                }
            }
            f >> line;
            for (i = 0; i < 100; i++)
            {
                str[i] = '\0';
            }
            line.copy(str, line.size() - 2, 0);
            line = str;
            //line += c;
            if (IsInput(line))
            {
                for (i = 0; i < SizeOfInput(); i++)
                {
                    if (input[i]->type == line)
                    {
                        input[i]->AddChild(p);
                    }
                }
            }
            else
            {
                for (i = 0; i < int (wires.size()); i++)
                {
                    if (wires[i]->type == line)
                    {
                        wires[i]->child = p;
                        break;
                    }
                }
            }
        }

	}
	f.close();
	/*
	for (i = 0; i < int(input.size()); i++)
	{
	    for (j = 0; j < int(wires.size()); j++)
	    {
	        if (input[i]->type == wires[j]->type)
	        {
	            wires[j]->parent = input[i];
	        }
	    }
	}
	*/
	for (i = 0; i < int(output.size()); i++)
	{
	    for (j = 0; j < int(wires.size()); j++)
	    {
	        if (output[i]->type == wires[j]->type)
	        {
	            wires[j]->child = output[i];
	        }
	    }
	}
	Node* nnode;
	for (i = 0; i < int(wires.size()); i++)
	{
	    p = wires[i]->parent;
	    nnode = wires[i]->child;
	    p->AddChild(nnode);
	}
	root = new Node;
	for (i  = 0; i < SizeOfOutput(); i++)
	{
	    root->AddParent(output[i]);
	}

}

void Scheme::ConvertInversionsToMarks()
{
	BFS(root, InversionToMarks);
}

void Scheme::ConvertMarksToInversions()
{
	BFS(root, MarksToInversion);
}

void InitMacro(Node * macro, char * path)
{
	ifstream f;
	f.open(path);
	string line;
	line = "start";
	int inCounter = 0;
	while (line != "endmodule")
	{
		f >> line;
		if (line == "input")
		{
			f >> line;
			inCounter += GetValue(line) + 1;
		}
		else if (line == "assign")
		{
			f >> line;
			f >> line;
			f >> line;
			f >> line;
			if (line == "&")
			{
				macro->type = "and";
			}
			else if (line == "~&")
			{
				macro->type = "nand";
			}
			else if (line == "|")
			{
				macro->type = "or";
			}
			else if (line == "~|")
			{
				macro->type = "nor";
			}
			else if (line == "^") // wtf is supposed to be here?
			{
				macro->type = "xor";
			}
			else if (line == "~^")
			{
				macro->type = "xnor";
			}
		}
	}
	f.close();
	for (int i = 0; i < inCounter; i++)
	{
		macro->parents.push_back(NULL);
	}
}

string IntToString(int  a)
{
    char c[20];
    sprintf(c, "%d", a);
    string res = c;
    return res;
}

string Scheme::GetInputName()
{
	int i, j;
	string line = "start";
	for (i = 0; i < SizeOfInput(); i++)
	{
		if (input[i]->is_visited == false)
		{
			line = input[i]->type;
			input[i]->is_visited = true;
			break;
		}
	}
	if (line == "start")
	{
		line = "done";
		return line;
	}
	else
	{
		if (line[line.size() - 1] == ']')
		{
			for (i = 0; i < line.size(); i++)
			{
				if (line[i] == '[')
				{
					line.erase(line.begin() + i, line.end());
					break;
				}
			}
			for (i = 0; i < SizeOfInput(); i++)
			{
				if (input[i]->is_visited == false)
				{
					for (j = 0; j < input[i]->type.size(); j++)
					{
						if (input[i]->type[j] == '[')
						{
							break;
						}
					}
					if (line.compare(0, line.size(), input[i]->type, 0, j) == 0)
					{
						input[i]->is_visited = true;
					}
				}
			}
		}
		return line;
	}
}

string Scheme::GetOutputName()
{
	int i, j;
	string line = "start";
	for (i = 0; i < SizeOfOutput(); i++)
	{
		if (output[i]->is_visited == false)
		{
			line = output[i]->type;
			output[i]->is_visited = true;
			break;
		}
	}
	if (line == "start")
	{
		line = "done";
		return line;
	}
	else
	{
		if (line[line.size() - 1] == ']')
		{
			for (i = 0; i < line.size(); i++)
			{
				if (line[i] == '[')
				{
					line.erase(line.begin() + i, line.end());
					break;
				}
			}
			for (i = 0; i < SizeOfOutput(); i++)
			{
				if (output[i]->is_visited == false)
				{
					for (j = 0; j < output[i]->type.size(); j++)
					{
						if (output[i]->type[j] == '[')
						{
							break;
						}
					}
					if (line.compare(0, line.size(), output[i]->type, 0, j) == 0)
					{
						output[i]->is_visited = true;
					}
				}
			}
		}
		return line;
	}
}

int Scheme::LetsCount(string line, string flag)
{
    int i, j, res = 0;
    if (flag == "in")
    {
        for (i = 0; i < SizeOfInput(); i++)
        {
            for (j = 0; j < input[i]->type.size(); j++)
            {
                if (input[i]->type[j] == '[')
                {
                    break;
                }
            }
            if (line.compare(0, line.size(), input[i]->type, 0, j) == 0)
            {
                res++;
            }
        }
    }
    else if (flag == "out")
    {
        for (i = 0; i < SizeOfOutput(); i++)
        {
            for (j = 0; j < output[i]->type.size(); j++)
            {
                if (output[i]->type[j] == '[')
                {
                    break;
                }
            }
            if (line.compare(0, line.size(), output[i]->type, 0, j) == 0)
            {
                res++;
            }
        }
    }
    return res;
}

vector<Wire *>  wiresnew;

int FindWire (Node * node, string line, int p)
{
    int i;
    if (line == "child")
    {
        //cout << node->type <<' '<<node->children[0]->type<<' ';
        for (i = 0; i <  wiresnew.size(); i++)
        {
            if (wiresnew[i]->p == node->uid)
            {
                //cout << wiresnew[i]->child->type << endl;
                return i;
            }
        }
    }
    else if (line == "parent")
    {

        for (i = 0; i <  wiresnew.size(); i++)
        {
            if ((wiresnew[i]->c == node->uid) && (wiresnew[i]->p == p))
            {
                return i;
            }
        }
    }
    return -1;
}

vector<string> vlines;
int numOfWires = 0;
int id = 0;

void Scheme::CreateVerilog(char * path)
{
    vector<string> namesin;
    vector<string> namesout;
    vector<int>    amountin;
    vector<int>    amountout;
	ofstream f;
	f.open(path);
	f << "module top(";
	string line;
	int i;
	for (i = 0; i < SizeOfInput(); i++)
	{
		input[i]->is_visited = false;
	}
	for (i = 0; i < SizeOfOutput(); i++)
	{
		output[i]->is_visited = false;
	}
	line = GetInputName();
	namesin.push_back(line);
	amountin.push_back(LetsCount(line, "in"));
    f << line;
	for (i = 1; i < SizeOfInput(); i++)
	{
		if (input[i]->is_visited == false)
		{
		    f << ", ";
			line = GetInputName();
			namesin.push_back(line);
            amountin.push_back(LetsCount(line, "in"));
			f << line;
		}
	}
	f << ", ";
	line = GetOutputName();
	namesout.push_back(line);
	amountout.push_back(LetsCount(line, "out"));
    f << line;
	for (i = 1; i < SizeOfOutput(); i++)
	{
		if (output[i]->is_visited == false)
		{
		    f << ", ";
			line = GetOutputName();
			namesout.push_back(line);
            amountout.push_back(LetsCount(line, "out"));
			f << line;
		}
	}
	f << ");" << endl;
	for (i = 0; i < amountin.size(); i++)
	{
	    f << "  input ";
	    if (amountin[i] > 1)
	    {
	        f << "[" << amountin[i] - 1 << ":0] ";
	    }
	    f << namesin[i] << ";" << endl;
	}
	for (i = 0; i < amountout.size(); i++)
	{
	    f << "  output ";
	    if (amountout[i] > 1)
	    {
	        f << "[" << amountout[i] - 1 << ":0] ";
	    }
	    f << namesout[i] << ";" << endl;
	}
	for (i = 0; i < amountin.size(); i++)
	{
	    f << "  wire ";
	    if (amountin[i] > 1)
	    {
	        f << "[" << amountin[i] - 1 << ":0] ";
	    }
	    f << namesin[i] << ";" << endl;
	}
	for (i = 0; i < amountout.size(); i++)
	{
	    f << "  wire ";
	    if (amountout[i] > 1)
	    {
	        f << "[" << amountout[i] - 1 << ":0] ";
	    }
	    f << namesout[i] << ";" << endl;
	}
	BFS(root, CountWires);
	for (i = 0; i < SizeOfInput(); i++)
	{
	    numOfWires -= input[i]->children.size();
	}
	numOfWires -= 2 * SizeOfOutput();
	f << "  wire [" << numOfWires - 1 << ":0] w;" << endl;
	wires.clear();
	BFS(root, NumberElements);
	BFS(root, UpdateWires);
	BFS(root, MakeLines);
	while (!vlines.empty())
	{
	    f << vlines.back() << endl;
	    vlines.pop_back();
	}
	f << "endmodule" << endl;
	f.close();
}
