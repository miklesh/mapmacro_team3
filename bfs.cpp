#include "scheme.h"
#include "bfs.h"

void PushInversion (Node *node)
{
	int i;
	if (node->parents[0] != NULL)
	{
		for (i = 0; i < node->InputPins(); i++)
		{
			if (node->parents[i]->OutputPins() == 1)
			{
				node->parents[i]->InvertType();
			}
			else
			{
				node->input_inversion[i] = true;
			}
		}
	}
	else
	{
		for (i = 0; i < node->InputPins(); i++)
		{
			node->input_inversion[i] = true;
		}
	}
}

extern vector<string> vlines;
extern int numOfWires;
extern vector<Wire *> wiresnew;
extern int id;

// Add other variations of xor and xnor. i.e. with v & V
Node* Visit(Node *node, int task, int set)
{
	int i;
	switch (task)
	{
		case (FindRoot):
			if (node->IsInSet(set))
			{
				for (i = 0; i < node->OutputPins(); i++)
				{
					if (node->children[i]->IsInSet(set))
					{
						return Visit (node->children[i], FindRoot, set);
					}
				}
				return node;
			}
			else
			{
				return NULL;
			}
		case (Free):
            delete(node);
            return NULL;
		case (And):
			if (!node->is_visited)
			{
				if (node->type == "and")
				{
				    //std::cout << "and " << node->uid<< " "<<node->num.size() << std::endl;
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_and, set);
				}
				else if (node->type == "nor")
				{
				    //std::cout <<"its nor" << node->uid<< " "<<node->num.size() <<std::endl;
					set = node->num.size();
					node->num.push_back(0);
					node->type = "and";
					PushInversion(node);
					Visit(node, build_and, set);
				}
			}
			return NULL;
		case (Nor):
			if (!node->is_visited)
			{
				if (node->type == "nor")
				{
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_nor, set);
				}
				else if (node->type == "and")
				{
					set = node->num.size();
					node->num.push_back(0);
					node->type = "nor";
					PushInversion(node);
					Visit(node, build_nor, set);
				}
			}
			return NULL;
		case (Or):
			if (!node->is_visited)
			{
				if (node->type == "or")
				{
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_or, set);
				}
				else if (node->type == "nand")
				{
					set = node->num.size();
					node->num.push_back(0);
					node->type = "or";
					PushInversion(node);
					Visit(node, build_or, set);
				}
			}
			return NULL;
		case (Nand):
			if (!node->is_visited)
			{
				if (node->type == "nand")
				{
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_nand, set);
				}
				else if (node->type == "or")
				{
					set = node->num.size();
					node->num.push_back(0);
					node->type = "nand";
					PushInversion(node);
					Visit(node, build_nand, set);
				}
			}
			return NULL;
		case (Xor):
			if (!node->is_visited)
			{
				if (node->type == "xor")
				{
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_xor, set);
				}
			}
			return NULL;
		case (Xnor):
			if (!node->is_visited)
			{
				if (node->type == "xnor")
				{
					set = node->num.size();
					node->num.push_back(0);
					Visit(node, build_xnor, set);
				}
			}
			return NULL;
		case (build_and):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "and")
					{
						Visit(node->parents[i], build_and, set);
					}
					else if (node->parents[i]->type == "nor")
					{
						node->type = "and";
						PushInversion(node->parents[i]);
						Visit(node->parents[i], build_and, set);
					}
				}
			}
			return NULL;
		case (build_nor):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "nor")
					{
						Visit(node->parents[i], build_nor, set);
					}
					else if (node->parents[i]->type == "and")
					{
						node->type = "nor";
						PushInversion(node->parents[i]);
						Visit(node->parents[i], build_nor, set);
					}
				}
			}
			return NULL;
		case (build_or):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "or")
					{
						Visit(node->parents[i], build_or, set);
					}
					else if (node->parents[i]->type == "nand")
					{
						node->type = "or";
						PushInversion(node->parents[i]);
						Visit(node->parents[i], build_or, set);
					}
				}
			}
			return NULL;
		case (build_nand):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "nand")
					{
						Visit(node->parents[i], build_nand, set);
					}
					else if (node->parents[i]->type == "or")
					{
						node->type = "nand";
						PushInversion(node->parents[i]);
						Visit(node->parents[i], build_nand, set);
					}
				}
			}
			return NULL;
		case (build_xor):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "xor")
					{
						Visit(node->parents[i], build_xor, set);
					}
				}
			}
			return NULL;
		case (build_xnor):
			node->is_visited = true;
			node->AddToSet(set);
			for (i = 0; i < node->InputPins(); i++)
			{
				if ((node->parents[i] != NULL) && (node->parents[i]->OutputPins() == 1))
				{
					if (node->parents[i]->type == "xnor")
					{
						Visit(node->parents[i], build_xnor, set);
					}
				}
			}
			return NULL;
		case (del):
			for (i = 0; i < node->InputPins(); i++)
			{
			    if (node->parents[i]->IsInSet(set))
			    {
			        Visit(node->parents[i], del, set);
			    }
			    node->DeleteParent(i);
			}
			//Visit(node, Free, set);
			return NULL;
		case (InversionToMarks):
			int j;
			if (node->type == "not")
			{
				for (i = 0; i < node->OutputPins(); i++)
				{
					for (j = 0; j < node->children[i]->InputPins(); j++)
					{
						if (node->children[i]->parents[j] == node)
						{
							node->children[i]->input_inversion[j] = true;
							node->children[i]->parents[j] = node->parents[0];
							node->parents[0]->children[0] = node->children[i];
							break;
						}
					}
				}
				delete(node);
			}
			return NULL;
		case (MarksToInversion):
			Node* p;
			for (i = 0; i < node->InputPins(); i++)
			{
				if (node->input_inversion[i] == true)
				{
					p = new Node;
					node->input_inversion[i] = false;
					p->type = "not";
					for (j = 0; j < node->parents[i]->OutputPins(); j++)
					{
						if (node->parents[i]->children[j] == node)
						{
							node->parents[i]->children[j] = p;
							p->parents.push_back(node->parents[i]);
							p->children.push_back(node);
							node->parents[i] = p;
							break;
						}
					}
				}
			}
			return NULL;
		case (CountWires):
			numOfWires += node->parents.size();
			return NULL;

        case (NumberElements):

			if ((node->type == "and")||(node->type == "nand")
                ||(node->type == "or")||(node->type == "nor")
                ||(node->type == "xor")||(node->type == "xnor")
                ||(node->type == "not"))
            {
                node->uid = id++;
            }
            else
            {
                node->uid = -1;
            }
        return NULL;

        case (UpdateWires):/*
        if ((node->type == "and")||(node->type == "nand")
                ||(node->type == "or")||(node->type == "nor")
                ||(node->type == "xor")||(node->type == "xnor")
                ||(node->type == "not"))
                {
                    for (i = 0; i < node->InputPins(); i++)
                    {
                        wiresnew.push_back(new Wire);
                        wiresnew.back()->type = IntToString(numOfWires--);
                        wiresnew.back()->child = node;
                        wiresnew.back()->parent = node->parents[i];
                    }
                }
        return NULL;*/
        if (node->uid >= 0)
        {
            for (i = 0; i < node->InputPins(); i++)
            {

                if (node->parents[i]->uid >= 0)
                {
                    wiresnew.push_back(new Wire);
                    //wiresnew.back()->type = IntToString(numOfWires--);
                    wiresnew.back()->c = node->uid;
                    wiresnew.back()->p = node->parents[i]->uid;
                    wiresnew.back()->id= numOfWires--;
                }

            }
        }
        return NULL;

        case (MakeLines):
            string line;
            int indx;
            line = "    ";
            if ((node->type == "and")||(node->type == "nand")
                ||(node->type == "or")||(node->type == "nor")
                ||(node->type == "xor")||(node->type == "xnor")
                ||(node->type == "not"))
                {
                    line += node->type;
                    line += " (";
                    indx = FindWire(node, "child");
                    if (indx < 0)
                    {
                        //std::cout << indx << endl;
                        line += node->children[0]->type;
                    }
                    else
                    {
                        line += "w[";
                        line += IntToString(wiresnew[indx]->id);
                        line += "]";
                    }
                    for (i = 0; i < node->InputPins(); i++)
                    {
                        line += ", ";
                        indx = FindWire(node, "parent", node->parents[i]->uid);
                        if (indx < 0)
                        {
                            line += node->parents[i]->type;
                        }
                        else
                        {
                            line += "w[";
                            line += IntToString(wiresnew[indx]->id);
                            line += "]";
                        }
                        //wiresnew[indx]->c = -1
                        //wiresnew.erase(wiresnew.begin()+indx);
                    }
                    line += ");";
                    vlines.push_back(line);
                }
            return NULL;
	}
	return NULL;
}

Node* BFS(Node *root, int task, int set)
{
	int i;
	queue<Node *> fringe;
	Node *cur;
	fringe.push(root);

	while (!fringe.empty())
	{
		cur = fringe.front();
		fringe.pop();
		//cur->is_visited = true;

		for (i = 0; i < cur->InputPins(); i++)
		{
			if (cur->parents[i] != NULL)
			{
				fringe.push(cur->parents[i]);
			}
		}
		cur = Visit(cur, task, set);
		if (cur != NULL)
		{
			return cur;
		}
	}
	return NULL;
}
