#include <iostream>
#include "scheme.h"
#include "bfs.h"
using namespace std;
int set_num = 1;

int main(int argc, char ** argv)
{
	int i;
    Scheme  Nashatyr;
    Node root, macro;
    root.type = "none";
    Nashatyr.Init(argv[1]); // Do parse, create scheme, change init prototype
    for (i = 0; i < Nashatyr.SizeOfOutput(); i++)
    {
    	output[i]->AddChild(root);
    }
    Nashatyr.ConvertInversionsToMarks();
    Nashatyr.root = &root;
    InitMacro(&macro, argv[2])
    Analise(root, Macro);   // Mark all possible variants
    Nashatyr.PlaceMacro(Macro);
    Nashatyr.ConvertMarksToInversions();
    Nashatyr.CreateVerilog(argv[3]);
    Nashatyr.FreeMemory();
    return 0;
}
